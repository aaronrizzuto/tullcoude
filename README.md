# README # !!WORK IN PROGRESS!!

Code for reducing Tull coude spectrograph data automatically. Comprised of multiple general functions for Echelle data that are applicable to multiple instruments.

Currently wavelength solution code needs work but other parts are functional. 

To run it: 

0. Run headstrip.py (manually change its target) to get data file header info into a csv table.

1. Set up the steps you want to run and assign the correct data file manually in go_coude.py

2. Create a directory in the same place as your data called "reduction", which is where reduction products will be saved.

3. Then just run it from the command line: python go_coude.py