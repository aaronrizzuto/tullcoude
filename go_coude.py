##General imports
import numpy as np
import pdb,glob,os,sys,pickle

##Other imports 
from astropy.io import fits
from readcol import readcol
from scipy.optimize import curve_fit
import scipy.signal
import scipy.spatial
from scipy.interpolate import UnivariateSpline as uvarspline

##My imports, for these you will need access to Aaron's utils repository
from mpfit import mpfit
from readcol import readcol
from smooth import smooth
from gaussfit import gaussfit_mp

##plotting
import matplotlib.pyplot as plt
import matplotlib as mpl
##set matplotlibglobal params for nicer looking plots
mpl.rcParams['lines.linewidth']   = 1.5
mpl.rcParams['axes.linewidth']    = 2
mpl.rcParams['xtick.major.width'] =2
mpl.rcParams['ytick.major.width'] =2
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['legend.numpoints'] = 1
mpl.rcParams['axes.labelweight']='semibold'


##A function that calculated equivalent width of lithium from a stretch of spectrum, 
##it's quick and dirty right now so it might need work 
def calc_ewli(wav,spec,sig_spec):

    rng = np.where((wav > 6706.7) & (wav <= 6708.43) )[0]
    wav = wav[rng]
    spec=spec[rng]
    sig_spec=sig_spec[rng]
    
    ##fit a gaussian to the line to centroid it and determine continuum level
    spot = np.argmin(spec)
    g0            = np.array([-np.median(spec)+np.min(spec),wav[spot],0.01,np.median(spec),0.0,0.0])
    gpars,gcov,gf = gaussfit_mp(wav,spec,sig_spec,g0)
    
    ##Define the continuum line based on the above fit
    cline = wav*gpars[4]+wav**2*gpars[5]+gpars[3]
    
    ##Trapezoidal integration of the pointwise equivalent width
    ewli = np.trapz(1-spec/cline,x=wav)
    dlam=wav[1]-wav[0]
    
    ##pointwise variance of the equivalent width
    var_ewli_pw = (sig_spec**2*(dlam*spec/cline**2)**2 + (dlam/cline)**2*sig_spec**2)

    ##sum and root for real uncertainty
    sig_ewli = np.sqrt(np.sum(var_ewli_pw))

    return ewli,sig_ewli
    
'''
plot all the cool things at once!! Mostly for testing things during observing nights
plots the H-alpha and Lithium orderes (hand coded, so wont work if trace is altered significantly)
plots whole spectrum
plot SNR estimate

wav is the wavelength solution  dimension (order,pixel)
spec is the spectra  (order,pixel)
sig_spec is the uncertainty on the spectra

optional SNR cut for removal from plots, default=20
'''
def allplot(wav,spec,sig_spec,snr=20):
    for i in range(wav.shape[0]):
        plt.plot(wav[i],spec[i]/sig_spec[i])
    plt.figure()
    for i in range(wav.shape[0]):
       # print i
       # pdb.set_trace()
        clean = np.where(spec[i]/sig_spec[i] > snr)[0]
        if len(clean) > 0:
            plt.plot(wav[i,clean],spec[i,clean])
    plt.figure()
    plt.errorbar(wav[29,:],spec[29,:],yerr=sig_spec[29,:])
    plt.errorbar(wav[30,:],spec[30,:],yerr=sig_spec[30,:])
    plt.show()
    
    
'''
simple function for diagnostic plotting
Plots every order of the spectrum a different colour
wav is the wavelength solution cube dimension (order,pixel)
spec is the spectra (order,pixel)
'''
def specplot(wav,spec):
    for i in range(wav.shape[0]): plt.plot(wav[i],spec[i])
    plt.show()
   
'''
As above but with a signal to noise cut
'''     
def cleanplot(wav,spec,sig_spec,snr=10):
    for i in range(wav.shape[0]):
       # print i
       # pdb.set_trace()
        clean = np.where(spec[i]/sig_spec[i] > snr)[0]
        if len(clean) > 0:
            plt.plot(wav[i,clean],spec[i,clean])
    plt.show()

'''
As above but plots signal to noise rather than the spectra
'''
def plotsnr(wav, spec,sig_spec):
    for i in range(wav.shape[0]):
        plt.plot(wav[i],spec[i]/sig_spec[i])
    plt.show()
       
        
'''
Compute a guassian, used for fitting with scipy curvefit function
'''
def _gaus(x,a,b,x0,sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))+b
    



def build_bias(fnames):
    """
    Build Bias
    Function that reads in bias images, then makes a superbias
    Input: 
        fnames: filenames of the bias frames
    Output: 
        median of the bias frame images
    """

    hdulist    = fits.open(fnames[0])
    head       = hdulist[0].header
    data       = hdulist[0].data
    bias_cube = np.zeros((len(fnames),data.shape[0],data.shape[1]))
    bias_cube[0] = data
    for i in range(1,len(fnames)):
        bias_cube[i] = fits.open(fnames[i])[0].data
        #print 'Reading Bias File ' + str(i+1) +" out of " + str(len(fnames))
    bias_final = np.median(bias_cube,axis=0)
    return bias_final

def build_flat_field(fnames,bias):
    '''
    Function that reads in flat lamp images, then makes a superflat
    Input: 
        fnames: filenames of the flat frames
        bias  : The superbias
    Output: 
        flat_final: the superflat (bias subtracted, medianed, normalized flat image) 
    '''
    hdulist    = fits.open(fnames[0])
    head       = hdulist[0].header
    data       = hdulist[0].data
    flat_cube = np.zeros((len(fnames),data.shape[0],data.shape[1]))
    flat_cube[0] = data - bias
    for i in range(1,len(fnames)):
        flat_cube[i] = fits.open(fnames[i])[0].data - bias
        #print 'Reading Flat File ' + str(i+1) +" out of " + str(len(fnames))
    flat_final = np.median(flat_cube,axis=0)
    flat_final-=np.min(flat_final)
    flat_final = flat_final/np.max(flat_final)
    #pdb.set_trace()
    return flat_final

##read regular images and arcs into cubes and do bias/flat correction
def make_cube(fnames,readnoise,dcur,bias=None,flat=None,bpmask=None):
    """
    Reads in general frame files (e.g. arc or target data), 
    performs bias subtraction and flat field division if asked to
    input:
        fnames: file names of the data files
        readnoise: readnoise value for the detector
        dcur:      dark current for the detector
    Options:
        bias: supply the superbias if you want to subtract it (maybe not for e.g. arcs)
        flat: supply the superbias if you want to do the flat correction (maybe not for e.g. arcs)
        bpmask: supply a mask of bad pixels on the detector if you want to set their SNR values to effective 0
    """
    ##read the frames in one-by-one
    for i in range(0,len(fnames)):
        print 'Reading ' + fnames[i]
        theimage = fits.open(fnames[i])[0].data
        if i == 0: ##if first frame, create storage arrays
            cube = np.zeros((len(fnames),theimage.shape[0],theimage.shape[1]))
            SNR  = np.zeros((len(fnames),theimage.shape[0],theimage.shape[1]))
        cube[i]  = theimage - dcur[0]
        SNR[i]   = cube[i]/np.sqrt(cube[i]+ dcur[0] + readnoise[0]**2)
        qwe = np.where(SNR < 0.0)[0]
        if len(qwe) >= 1: 
            print 'Theres something wrong here, Ive got negative signal to noise!!!!'
            import pdb
            pdb.set_trace()

        ##if bias/flat supplied, do the corrections
        if bias != None: cube[i] = cube[i]-bias 
        if flat != None: cube[i] = cube[i]/flat
        ##set SNR on bad pixels to zero if asked to 
        if bpmask != None: 
            cube[i,bpmask[0],bpmask[1]] = np.median(cube[i])
            SNR[i,bpmask[0],bpmask[1]]  = 0.001 ##effective 0

        ##correct nans, this is where there is no signal in the flat field or the image.
        nnn = np.where(np.isnan(cube[i]))
        SNR[i,nnn[0],nnn[1]] = 0.001
        cube[i,nnn[0],nnn[1]] = 1.0
        
    return cube,SNR

def make_badpix_mask(bias,flat,cutlevel = 99.8, showit=True):
    '''
    Build a mask of bas pixel locations from the superbias using some simple stats
    input:
        superbias: the superbias image
        flat:      the superflat image
        options: 
            cutlevel: the percentile cut, default 99.8 seems reasonable
            showit: if True (default), plots that mask for user veiwing
        output:
            bad: The bad pixel mask, it's a tuple of 1-d numpy ndarrays, corresponding to (x,y) pixel coords
    '''
    ppp = np.percentile(bias,cutlevel)
    bad = np.where((bias > ppp) | (flat <= 0.0001))
    if showit == True:
        plt.imshow(np.log10(bias),interpolation='none',aspect='auto')
        plt.plot(bad[1],bad[0],'r,') ## inverting x/y here because of the behaviour of imshow
        print "Here is the bad pixel map over the superbias"
        plt.show()
    return bad

def start_trace(data):
    '''
    Uses the flat field image to estimate starting pixel values for the order trace fits

    input:
        data: the a 1-d array along the superflat image going accross the orders
    Output:
        order_zeros: positions along data of orders
        ovals: The values at those points
    '''
    ##compute the numerical gradiant
    fgrad = np.gradient(data)
    ##the cut level to get all the reasonable orders needs automation of significant improvement
    cutval=4.0e-5 ##this is what I used to the 48 orders
    ##ask the user for the value
    print "Choose and fgrad cut value"
    plt.plot(fgrad)
    plt.show()
    numnum = input("Choose cut value:  ")
    cutval = numnum
    order_zeros = ()
    ovals = ()
    last = 0
    ##run along the gradient and find the shapes corresponding to orders
    for i in range(6,data.shape[0]):
    ##if grandient above cut level
       if (fgrad[i] > cutval) | (last == 0):
            ##if we've gone at least 20 pixels then call this a new order
            if (i-last > 20) | (last ==0):
            ##append the positions plus an offset to get to the middle of the order
                order_zeros +=(i+11,)
                ovals += (data[i+11],)
                last = i
    order_zeros = np.array(order_zeros)
    ovals       = np.array(ovals)
    return order_zeros,ovals
    

def full_trace(cube,zerotrace,orderstart):
    """
    Do a full trace along the orders based on an input starting trace
    Inputs:
        cube: cube of images you want to use for the order trace, pick a really bright object with
        lots of photons. e.g. an RV standard or A0 star.
    
        zerotrace: positions of orders at the first pixel orderstart from start_trace function
    
        orderstart: The pixels where the data starts (to skip the overscan region)
    """
    nord     = len(zerotrace)
    tracedat = np.zeros((cube.shape[0],nord,cube.shape[2]+orderstart))
    ##loop over each object frame
    for frm in range(cube.shape[0]):
        ##now loop over each pixel
        for pix in range(1,cube.shape[2]+orderstart+1):
            prev = zerotrace
            if pix > 1: prev = tracedat[frm,:,-pix+1]
            m1d = obj_cube[frm,:,-pix + orderstart]
            ##now loop over orders
            for ord in range(nord):
                edge1 = prev[ord]-3
                if edge1 < 0: edge1 = 0
                ##basically just finding the maximum across the order at this pixel position
                ##the fit will be improved later in the extraction process
                tracedat[frm,ord,-pix] = edge1+np.argmax(m1d[edge1:edge1+6])
                ##the non detection case, make sure it doesnt travel to a new order over a bunch of pixels
                ##after the first pixel, it should never move more than a few pixel at a time
                if pix != 1:
                    if (tracedat[frm,ord,-pix] > prev[ord]+2) | (tracedat[frm,ord,-pix] < prev[ord]-2): tracedat[frm,ord,-pix]=prev[ord]
        print "tracing object frame " + str(frm+1) + " out of " + str(cube.shape[0])
    return tracedat


def order_resid(p,xx=None,yy=None,image=None,err=None,model=False,fjac=None):
    '''
    Function in the mpfit style to compute a model order shape 
    its a gaussian at each pixel row, with peak heights, positions, and widths varying parabolically

    p is the input parameters

    returns residuals for mpfit unless model=True is set, then it returns information for the user to eyeball things
    '''
    ##order trace residual (parabola)
    mvect  = p[2]*xx**2 + p[1]*xx + p[0]
    ##peak shape curve
    peak   = p[3] + p[6]*xx + p[7]*xx**2
    ##sigma curve
    sigmas = p[4]+p[8]*xx**1 + p[9]*xx**2 + p[10]*xx**3
    ##actual model
    mmm    = peak*np.exp(-(yy-mvect)**2/2/sigmas**2) + p[5]
    resid_2d = (image-mmm)/err
    resid    = resid_2d.flatten()
    status=0
    #pdb.set_trace()
    if model == True: return mmm,mvect,resid_2d,peak,sigmas
    if model == False: return [status,resid]


##extract spectra from the data cubes according to the order trace
def extractor(cube,cube_snr,trace,quick=True,arc=False,nosub=True):
    '''
    Main code to turn data images into a series 1d-spectra (one per order)
    Input:
        cube: the datacube of image frames
        cube_snr: corresponding SNR cube
        trace: The order position trace on the ccd, computed previously 
    
    Options:
        quick: if True, does a simple baground minimum subtractions and skips slow fitting process,
                good for quicklook or testing the pipelines other functions. If False, full fit is done.
        arc: set to True for extracting arc frames, should also set quick to True
    
        nosub: if True, no background subtraction carried out at all, mainly a diagnostic tool
    
    Outputs: 
        flux: cube (frames,orders,pixel) containing the extracted raw spectra
        error: corresponding uncertainties (same shape)
    
    General Overview of Steps:

    for each order in each data frame:
    


    '''
    flux  = np.zeros((cube.shape[0],trace.shape[0],trace.shape[1]))
    error = flux*0.0
    ##go frame by frame
    for frm in range(cube.shape[0]):
        #frm=-1 ##set a frame here for testing
        print "Extracting Frame " + str(frm+1) +" out of " + str(cube.shape[0])
        thisfrm = cube[frm,:,:]
        thissnr = cube_snr[frm,:,:]
        
        ## go order by order 
        for ord in range(trace.shape[0]):
            #ord=21 set an order here for testing

            ##fix the order trace shape first, to account for random pixel shifts 
            ###!!!DANNY this could really be moved to the trace functions as an additional step
            ##rather than doing it multiple times here!!!!!
            tracepars = np.polyfit(range(trace.shape[1]),trace[ord,:],2)
            tracepoly = np.polyval(tracepars,range(trace.shape[1]))
            trace[ord] = tracepoly
            
            #now get the whole trace in a block
            tblock = np.zeros((trace.shape[1],16))
            tsnr   = tblock.copy()
            x      = np.arange(tblock.shape[0])
            y      = np.arange(tblock.shape[1])
            xx,yy  = np.meshgrid(x,y)
            xx     = xx.T
            yy     = yy.T
            ##cut out a region around the trace for this order
            for pix in range(trace.shape[1]):  
                tblock[pix,:] = thisfrm[np.round(trace[ord,pix])-8:np.round(trace[ord,pix])+8,pix]
                tsnr[pix,:]   = thissnr[np.round(trace[ord,pix])-8:np.round(trace[ord,pix])+8,pix]
##a diagnostic plot for testing
#             plt.imshow(thisfrm[trace[ord,1350]-8:trace[ord,1350]+8,1330:1360],aspect='auto')
#             plt.plot(range(30),trace[ord,1330:1360]-trace[ord,1350]+9)
#             test = thisfrm[trace[ord,1330:1360]
#             pdb.set_trace()

            ##if not running the quick mode
            if (quick == False) & (arc == False):   

                ##clean obvious high outliers 
                trybad = np.where(tblock > 10*np.median(tblock))
                tsnr[trybad[0],trybad[1]] = 0.000001
                tblock[trybad[0],trybad[1]] = np.median(tblock)
                err = np.absolute(tblock/(tsnr))

                ##clean zero values (often due to chip artifacts that aren't caught)
                badpoints = np.where(tblock <= 0)
                tblock[badpoints[0],badpoints[1]] = np.median(tblock)
                err[badpoints[0],badpoints[1]] = np.median(tblock)*2.0
                tsnr[badpoints[0],badpoints[1]] = 0.00001                
                
                ##set up a fit of the order position on the trace block
                fa  = {'xx':xx, 'yy':yy, 'image':tblock,'err':err}
                ##initialize the normalizations paramaters to the centre values of tblock
                p0  = np.array([np.argmax(tblock[0,:]),0.0,0.0,np.median(tblock[:,np.argmax(tblock[1000,:])]),2.0,np.percentile(tblock,2),0.0,0.0,0.0,0.0,0.0]) 
                thisfit2d    = mpfit(order_resid, p0, functkw=fa,quiet=True) 
                if thisfit2d.status == -16: 
                    print 'mpfit for order fialed!, not sure what happened???!!!!???'
                    pdb.set_trace()
                ##the best fit parameters
                orderpars    = thisfit2d.params
                ##the corresponding best model  
                bestmod,besttrace,bestresid,bestpeak,bestsigmas    = order_resid(orderpars,xx=xx,yy=yy,image=tblock,err=tblock/tsnr,model=True)
                
                ##now remove points that are obvious outliers from the best model
                cutresid    = np.percentile(bestresid.flatten(),99.99)
                badcut      = np.where(bestresid > cutresid)
                tblock[badcut[0],badcut[1]] = np.median(tblock)
                tsnr[badcut[0],badcut[1]]   = 0.00001
                thistrace   = besttrace[:,0].copy()
                thispeak    = bestpeak[:,0].copy()
                thissigma   = bestsigmas[:,0].copy()
                peakshape   = np.sum(bestmod,axis=1)
                block_bg    = orderpars[5]
                block_sigma = orderpars[4]

            
            ## now that a starting order shape fit is done, can go pixel-by-pixel across the order
            ## and do a more detailed fit
            
            ##a quick print update so I don't get antsy            
            print "Extracting Ord " + str(ord+1) +" out of " + str(trace.shape[0]) + " for frame " + str(frm+1) + " of " + str(cube.shape[0])


            savesigma = np.zeros(trace.shape[1])
            savepeak  = np.zeros(trace.shape[1])
            savebg    = np.zeros(trace.shape[1])
            savespot  = np.zeros(trace.shape[1])
            #go pixel-by-pixel along the order
            for pix in range(trace.shape[1]):
                ##pix = 1199 #set a pixel here for testing
                slice     = tblock[pix,:]
                slice_snr = tsnr[pix,:]
                thisx     = np.arange(len(slice))
                snoise = np.absolute(slice/slice_snr)        
                qwe       = np.where(slice_snr < 0.0)[0]
                if len(qwe) >= 1: 
                    print 'Slice SNR is bad, bugshoot!!!'
                    pdb.set_trace()
                
                ##decide which extraction we do, e.g. a fast one, a detailed fit, or just get arc lines
                if quick == False: ##the detailed fit case
                    tspot     = thistrace[pix]
                    tpeak     = thispeak[pix]
                    tsigma    = thissigma[pix]

                    ##set up an mpfit of the order profile
                    ##initial parameters: peak height, peak centroid and peak width initalized from global order fit above, 
                    p0 = np.array([tpeak,tspot,tsigma,block_bg])  

                    ##limit some parameters, e.g. the width and centroid locations shoulndt travel far
                    ###this is mostly needed for the case of low-snr data where the peak is hard to pick out  
                    parinfo = [{'fixed':0, 'limited':[0,0], 'limits':[0.,0.]} for i in range(4)]
                    parinfo[1]['fixed']   = 0
                    parinfo[2]['fixed']   = 0
                    parinfo[1]['limited'] =[1,1]
                    parinfo[1]['limits']  =[p0[1]-1.0,p0[1]+1.0]
                    parinfo[2]['limited'] = [1,1]
                    parinfo[2]['limits']  = [p0[2]-1.0,p0[2]+2.0]
                    fa = {'x':thisx, 'y':slice, 'err':slice/slice_snr}

                    ##the call to the fitter
                    thisfit    = mpfit(profile_resid, p0, functkw=fa,quiet=True,parinfo=parinfo)    
                    #best fit parameters, save them
                    fitpars    = thisfit.params
                    savesigma[pix] = fitpars[2]
                    savespot[pix]  = fitpars[1]
                    savepeak[pix]  = fitpars[0]
                    savebg[pix]         = fitpars[3]
                    bg         = fitpars[3]

                    #best model and residuals
                    resids = profile_resid(fitpars,x=thisx,y=slice,err=slice/slice_snr,model=False,fjac=None)[1]
                    themod = profile_resid(fitpars,x=thisx,y=slice,err=slice/slice_snr,model=True,fjac=None)

                    ##detect outlier where residuals are really large
                    ##the cut value might need more thought 
                    bads = np.where(np.absolute(resids)>20)[0]
                    slice_snr[bads] = 0.0001 ##set their SNR to effective zero
                    if len(bads) > 3: slice_snr = slice_snr*0.000+0.0001 ##if there are three bad points of more, kill the while pixel position

                ## if quick is true, simple minimum background
                if quick   == True: bg = slice*0.0 + np.min(np.absolute(slice))
                if nosub   == True: bg = slice*0.0 ##case for no subtraction at all (e.g., arcs)
                cslice  = slice - bg ## subtract the background, whatever the case was

                ##sum up across the order, just a SNR weighted mean
                flux[frm,ord,pix]  = np.sum(slice_snr*cslice)/np.sum(slice_snr)

                ##if this is an arc frame, don't do anything fancy for calculating the spectrum
                if arc == True: flux[frm,ord,pix] = np.sum(cslice)
                
                ##the uncertainty
                final_sig = np.sqrt(np.sum(slice_snr**2*snoise**2)/np.sum(slice_snr)**2)
                final_snr = np.absolute(flux[frm,ord,pix]/final_sig)

                if final_snr <0: 
                    print 'something has gone wrong, negative errors!!??!!'
                    pdb.set_trace()            
    
                error[frm,ord,pix] = final_sig*1.0
    
                ##clean bad points from chip artifacts
                badlow = np.where(flux[frm,ord] <=1)
                error[frm,ord,badlow] = np.median(flux[frm,ord])

    return flux,error

def profile_resid(p,x=None,y=None,err=None,model=False,fjac=None):
    '''
    mpfit style function of the order profile model, for doing the detailed extraction and 
    background removal

    The model is a gaussian with a constant level, 

    improvement idea: add a linear background across order? might not be needed

    if model=True, returns model to user, otherwise returns residuals for mpfit
    '''
    mmm = p[0]*np.exp(-(x-p[1])**2/2/p[2]**2)+ p[3]
    resid = (y-mmm)/err
    status=0
    #pdb.set_trace()
    if model == True: return mmm 
    if model == False: return [status,resid]
 
 
   
def find_peaks(wav, spec, pwidth=10, plevel=97, minsep=1):
    '''
    Automatic peak finding for wavelength solutions. Use for a starting order that you known is super clean.
    Mostly wont work because there are bleed over lines from other orders that mess things up terribly
    !!!DANNY This might be a starting point for something automatic in the future

    INput:
        wav: wavelenghts (can be a dummy array if you like)
        spec: the spectrum
    
        pwidth: expected peak size
        plevel: percentile level for identifying a peak
        minsep: minimum separation between peaks
    
        Output:pcent_pix,wcent_pix, the pixel position and wavelength position of the peaks
    '''

    ##given a slice of wavecal spectrum, find line peaks above some significance level    
    flux_thresh = np.percentile(spec,plevel)
    high        = np.where((spec >= flux_thresh))[0]
    
    ##get peaks seperated by pwidth
    pk = ()
    for i in range(len(high)):
        #pdb.set_trace()
        maxspot = np.argmax(spec[np.max([high[i]-pwidth,0]):np.min([high[i]+pwidth,len(spec)-1])])
        spot = maxspot+high[i]-pwidth
        pk += (spot,)
    pk = np.array(pk)
    pk = np.unique(pk)

    ## offset from start/end of array by at least same number of pixels
    pk = pk[pk > pwidth]
    pk = pk[pk < (len(spec) - pwidth)]
    pcent_pix = np.zeros_like(pk,dtype='float')
    wcent_pix = np.zeros_like(pk,dtype='float') 
    ## for each peak, fit a gaussian to find center
    for i in range(len(pk)):

        xi = wav[pk[i] - pwidth:pk[i] + pwidth]
        yi = spec[pk[i] - pwidth:pk[i] + pwidth]
        
        pguess = (np.nanmax(yi), np.median(spec), float(np.nanargmax(yi)), 2.)
        try:
            popt,pcov = curve_fit(_gaus, np.arange(len(xi),dtype='float'), yi,p0=pguess)

            ## the gaussian center of the line in pixel units
            pcent_pix[i] = (pk[i]-pwidth) + popt[2]
            ## and the peak in wavelength units
            wcent_pix[i] = xi[np.nanargmax(yi)]

        except RuntimeError:
            pcent_pix[i] = float('nan')
            wcent_pix[i] = float('nan')

    wcent_pix, ss = np.unique(wcent_pix, return_index=True)
    pcent_pix = pcent_pix[ss]
    okcent = np.where((np.isfinite(pcent_pix)))
    wcent_pix = wcent_pix[okcent]
    pcent_pix = pcent_pix[okcent]
    vals = spec[pcent_pix.astype(int)]
    oks  = np.zeros(len(pcent_pix),int)+1

    ##now make sure nothing is closer than minsep in wavelength units
    for i in range(len(wcent_pix)):
        dist = np.absolute(wcent_pix - wcent_pix[i])
        close = np.where(dist <= minsep)[0]
        small = np.where(vals[close] < np.max(vals[close]))[0]
        if len(small) != 0: oks[close[small]] = -1
    keep = np.where(oks == 1)
    pcent_pix = pcent_pix[keep]
    wcent_pix = wcent_pix[keep]
    
    return pcent_pix,wcent_pix
 
def fit_peaks(wav,spec,peaks,pwidth=1.0):
    '''
    given a user-clicked peak position in a spectrum, find the real centroid with a gaussian fit 
    input:
        wav: the wavelengths (1-D) 
        spec: spectrum (1-D)
        peaks: peak positions
        pwidth: peak size default 1 angstrom
    '''
    wpeak = np.zeros(len(peaks))
    ppeak = np.zeros(len(peaks))
    for i in range(peaks.shape[0]):
        print "centroiding peak" + str(i)
        rng = np.where((wav >= peaks[i]-pwidth) & (wav <= peaks[i]+pwidth))[0]
        xi = rng.astype(float)   
        wi = wav[rng]
        yi = spec[rng]        
        p0 = (np.nanmax(yi), np.median(spec), xi[np.nanargmax(yi)], 2.)
        popt,pcov = curve_fit(_gaus, xi, yi,p0=p0)
        wpeak[i] = np.interp(popt[2],xi,wi)
        ppeak[i] = popt[2]
    return wpeak,ppeak
    


def nearest_lines(x,xpix,lampx,maxdist=5.0):
    '''
    Used by auto wavecal for initial order starting fit
    Given rough wavelength positions of some lines in the standard lamp spectrum
    find the nearest arc line from the arc exposure, removing any lines that are too far from 
    anything.
    Only ever assigh one lamp line to one chosen peak.
    input: 
        x: data peak locations in wavelength
        xpix: data peak locations in pixels
        lampx: lamp peak locations in wavelength
        maxdist: default 5 angstroms
    output:
        outx: wavelength location on lines matched
        outl: wavelength location of lamp library spectrum lines
        outp: pixel location of lines matched
    '''

    donex = np.zeros(len(lampx))
    donep = np.zeros(len(lampx))
    d_ind = np.zeros(len(lampx),int)-1
    for i in range(len(x)):
        thisdist = np.absolute(lampx-x[i])
##        find the closest lamp line
        close = np.where(thisdist == min(thisdist))[0]
        if len(close) > 1: pdb.set_trace()
        if (thisdist[close] <= maxdist) & (d_ind[close] == -1):
            d_ind[close] = i
            donep[close] = xpix[i]
            donex[close] = x[i]
        if (thisdist[close] <=maxdist) & (d_ind[close] != -1):
            newdist = lampx[close] - donex[close]
            if newdist > thisdist[close]: 
                d_ind[close]  = i
                donep[close] = xpix[i]
                donex[close] = x[i]
    ##recover the found lamp lines
    found = np.where(d_ind != -1)[0]
    d_ind = d_ind[found]
    outx = donex[found]
    outp = donep[found]
    outl = lampx[found]
    return outx,outl,outp


def fit_wsol(x,lampx,npix,degree=3):
    '''
    fit a polynomial to arc and lamp-file line positions
     x: pixels
     lampx: lamp wavelength positions
     npix: number of pixels
    '''
    fitpars = np.polyfit(x,lampx,degree)
    wsol    = np.polyval(fitpars,np.arange(npix))
    return fitpars,wsol


def order_wcal(roughwav,ord,lamp_wav,lamp_spec,diagnostics=False):
    '''
    automatic wavelength calibration that finds peaks in the lamp and arc spectra
    using statistics. !!only use for starting order (number 32) that you know is super 
    clean and doesn't have bleed-over lines to get a zeroth order wavecal to start from!! 

    input :
        roughwav: approximate wabelength solution accross order
        ord: arc spectrum for this order
        lamp_wav: lamp file wavelengths
        lamp_spec: lamp file specturm (i.e. not tull data but a spectrum thats calibrated)
    output:
        polynomial fit params, wavelengths solution, number of lines, starting wavelength solution
    '''
    ##zero things for ease of use
    ls = lamp_spec - np.min(lamp_spec)
    ooo = ord -np.min(ord)
    
    ##identify peaks in the arc 
    pspec99,wspec99   = find_peaks(roughwav,ord,plevel=97,minsep=10.0)
    ##identify peak in the lamp spectrum file
    plamp95,wlamp95 = find_peaks(lamp_wav,lamp_spec,plevel=95,minsep=10.0)
    
    ##now get the nearest wavelengths in the lamp file to these line positions
    outx,outl,outp = nearest_lines(wspec99,pspec99,wlamp95,maxdist=8.0)
    if len(outx) <2: 
        print 'not enough lines for initial fit!!!'
        pdb.set_trace()
        
    ##first fit a linear solution, to get a starting wavelength solution
    fitpars,wsol0 = fit_wsol(outp,outl,len(ord),degree = 2)
    
    ##now that we have a basic wavelength solution, do a better version, including finding more peaks
    pspec,wspec     = find_peaks(wsol0,ord,plevel=97,minsep=10)

    ##search the appropriate range of lamp spectrum to get more/better lines
    outx2,outl2,outp2 = nearest_lines(wspec,pspec,wlamp95,maxdist=2.0)
    if len(outx2) < 4: 
        print 'not enough lines for second fit'
        pdb.set_trace()
    ##the better fit (4th order)
    fitpars2, wsol = fit_wsol(outp2,outl2,len(ord),degree = 4)

    ##stop here if testing stuff
    if diagnostics == True: pdb.set_trace()
    return fitpars2,wsol,len(outl2),wsol0
    

def translate_peaks(pos,linelist):
    '''
    Once a lamp line has been user-clicked and centroided, figure out it's real position
    from the line list, also, remove it if it's not in the line list for some reason
    '''
    tranpeak = np.zeros(len(pos))
    for i in range(len(pos)):
        distance    = np.absolute(linelist-pos[i])
        thespot     = np.argmin(distance)
        if np.min(distance) >  1.0: tranpeak[i] = -1 
        if np.min(distance) <= 1.0: tranpeak[i] = linelist[thespot]
    return tranpeak
        
class plotpoint():
    '''
    A class for handling listening for user clicks on matplotlib plots and passing the 
    coordinates for use in the wavelength calibration.
    '''
    def __init__(self,xx,yy,xx2,yy2,extrax=None,extray=None):
        self.xx = xx
        self.yy = yy
        self.xx2 = xx2
        self.yy2 = yy2
        self.extrax = extrax
        self.extray = extray
        self.clickpoints = []
        
    def getcoord(self):
        fig=plt.figure(figsize=(20,12))
        ax = fig.add_subplot(111)
        limit1 = -100.0
        limit2 = np.max([np.max(self.yy)/3.0,3000.0])
        plt.plot(self.xx,self.yy,'b')
        plt.plot(self.xx2,self.yy2,'r')
        plt.ylim([limit1,limit2])
        if self.extrax != None:
            plt.plot(self.extrax,self.extray,'ro')
        cid = fig.canvas.mpl_connect('button_press_event', self.__onclick__)
        plt.show()
        return self.clickpoints

    def __onclick__(self,click):
        point = [click.xdata,click.ydata]
        print click.xdata,click.ydata
        return self.clickpoints.append(point)
    
 
def reshape_wavecal(template_wsol,template_pars,numarcs):
    '''
    if a previous night that you are using as a starting wavcal has less arc exposures, reformat 
    its wsol array by copying it into the right shape 
    this is used in the code preamble below and is a user-beware section that should generally 
    be totally commented out.   
    '''
    wsol_start  = np.zeros((numarcs,template_wsol.shape[0],template_wsol.shape[1]))
    pars_start  = np.zeros((numarcs,template_pars.shape[0],template_pars.shape[1]))
    for i in range(numarcs):
        wsol_start[i] = template_wsol.copy()
        pars_start[i] = template_pars
    pickle.dump(wsol_start,open(rdir+"global_wsol.pkl","wb"))
    pickle.dump(pars_start,open(rdir+"params_wsol.pkl","wb"))    
    return wsol_start,pars_start


def order_wcal_interact(roughwav,ord,lw,lamp_spec,linelist):
    '''
    interactive wavelength calibration for a single order
    ask user to match standard lamp and arc exposure lines, and then handles centroiding line 
    peaks and fitting the wavelength solution.
    Inputs: 
        roughwav: estimate of wavelength solution
        ord: the arc spectrum for the order
        lw: lamp wavelength
        lamps_spec: lamp spectrum from NOAO
        linelist: the corresponding linelist to lamp_spec/lw
    
    Outputs:
        fitpars: the wavelength solution polynomial coeffs
        wsol: the wavelength solution 
        outpeaks: data/lampfile peak positions for use in future
    '''
    ##scale things to make plotting clearer, this doesn't always work well
    ls    = lamp_spec
    divisor = np.max(ord)
    ord   = ord*np.max(ls[200:-200])/divisor*5
    
    okcal = 0
    ##loop while the wavelength calibration isn't done yet
    while okcal != 1:
        ##user clickes arc lines they want to calibrate to
        print "click lines on the lamp (blue) spectrum then close the plot"        
        lpoint = plotpoint(lw,ls,roughwav,ord)
        lpoint.getcoord()
        l_lines       = np.array(lpoint.clickpoints)

        ##centroid the clicked peaks
        lwpeak,lppeak = fit_peaks(lw,ls,l_lines[:,0],pwidth=0.5)
   
        ##find them on the full linelist for exact positions
        lwpeak_final  = translate_peaks(lwpeak,linelist)
   
        ##remove any lines not in the line list
        goodline = np.where(lwpeak_final >= 0.0)
        badline  = np.where(lwpeak_final < 0.0)

        if len(badline) != 0: 
            print "Some chosen lines not in line list!"
            print str(lwpeak[badline])
            lwpeak       = lwpeak[goodline]
            lppeak       = lppeak[goodline]
            lwpeak_final = lwpeak_final[goodline]
            l_lines      = l_lines[goodline]

        ##now user clicks corresponding arc spectrum lines
        d_lines = np.zeros((l_lines.shape[0],2)) 
        for i in range(0,l_lines.shape[0]):
            print "click corresponding Line on the data (red) spectrum then close the plot"
            spoint = plotpoint(lw,ls,roughwav,ord,extrax=lwpeak_final[i],extray=l_lines[i,1])
            spoint.getcoord()
            d_lines[i] = np.array(spoint.clickpoints)
        ##centroid these peaks too
        dwpeak,dpeak = fit_peaks(roughwav,ord,d_lines[:,0],pwidth=0.5)
   
        ##now run the wavelength solution
        fitpars,wsol = fit_wsol(dpeak,lwpeak_final,len(ord),degree = 4)
   
        ##ask the user if they are happy with this
        plt.plot(lw,ls,'b')
        plt.plot(wsol,ord,'r')
        print "Is the wavecal ok?"
        plt.show()
        okcal = input('Well is it? 1=yes,0=no ')
    
    
    ##once the user says it's all good, output everything
    ##also output the peak positions, for future use in speeding things up
    ##this is outputting the lamp wavelength positions corresponding to the arc spectrum pixel positions
    outpeaks = np.transpose(np.array([dpeak,lwpeak_final]))
    return fitpars,wsol,outpeaks

##Wavelength calibration in the case of known lines/peaks, e.g. a second arc lamp exposure after first calibration done
##Or checking through a calibration to make sure it's good. It will always refit the solution, even if you tell it everything is fine
def order_wcal_repass(roughwav,ord,lw,lamp_spec,linelist,peaks,trustit=False):
    '''
    Wavelength calibration in the case of known lines/peaks, e.g. a second arc lamp 
    exposure after first calibration done, Or checking through a calibration to make 
    sure it's good. It will always refit the solution, even if you tell it everything is fine
    but you wont have to click things.

    input:
        roughwav: estimated wavelegth solutions (the previous fit)
        ord: arc spectrum
        lw: lamp wavelegnths
        lamp_spec: lamp library spectrum
        linelist: lamp line list
        peaks: peak positions used in previous fit
    
    Optional:
        trustit: if True, don't even show the user anything, just trust the peaks are right

    Outputs:
        fitpars: the wavelength solution polynomial coeffs
        wsol: the wavelength solution 
        outpeaks: data/lampfile peak positions for use in future
    '''
    ##scale things to make plotting clearer
    ls  = lamp_spec*1.0
    ord = ord*np.max(ls)/np.max(ord)
    ##unpack the known peak locations
    klpeakw = peaks[:,1]
    kdpeak  = peaks[:,0]
    
    ##does the user want to check everything?
    if trustit == False:
        plt.plot(lw,ls,'b')
        plt.plot(roughwav,ord,'r')
        plt.plot(roughwav[kdpeak.astype(int)],ord[kdpeak.astype(int)],'ro')
        plt.plot(klpeakw,klpeakw*0.0+np.median(ls),'go')
        plt.errorbar(roughwav[kdpeak.astype(int)],ord[kdpeak.astype(int)],xerr =kdpeak*0.0+0.5,fmt='.r')
        print 'Are the peaks in the correct positions?'
        plt.show()
        okpeaks  = input('Well are they? 1=yes,0=no ')
    if trustit == True: okpeaks = 1
    ##if no, restart everything from scratch for this order by running the interactive wavecal
    if okpeaks != 1:
        fitpars,wsol,outpeaks = order_wcal_interact(roughwav,ord,lw,ls,linelist)
        return fitpars,wsol,outpeaks
    
    ## if peaks look good, recentroid and try the fit again
    ##recentroid on the arc spectrum peaks
    dwpeak,dpeak  = fit_peaks(roughwav,ord,roughwav[kdpeak.astype(int)],pwidth=0.5)
    ##now do the wavelength solution
    fitpars,wsol = fit_wsol(dpeak,klpeakw,len(ord),degree=4)
    ## give an estimate of how much the wavelength solution changed from the previous solution
    print "Refit Delta " + str(np.median(roughwav-wsol))

    ##show the wavelength solution if the user wants to check things
    if trustit == False:
        plt.plot(lw,ls,'b')
        plt.plot(wsol,ord,'r')
        print 'Is the wavecal ok?'
        plt.show()
        okpeaks = input('Well is it? 1=yes,0=no ')
    if trustit == True: okpeaks = 1
    ##if bad, start again from scratch for this order
    if okpeaks != 1: 
        fitpars,wsol,outpeaks = order_wcal_interact(roughwav,ord,lw,ls,linelist)
        return fitpars,wsol,outpeaks
    
    ##if good, return the current solution, it's always refit, but with the same peaks.
    return fitpars,wsol, np.transpose(np.array([dpeak,klpeakw]))
        
def wavecal(spec,sig_spec,lamp_wav,lamp_spec,linelist,upto=(-1,-1),trust_upto=False):
    '''
    Central wavelength calibration function that does cleaning and call either
    order_wcal_interact() or order_wcal_repass() functions

    input:
        spec: extracted arc spectra cube (frame,order,pixel)
        sig_spec : corresponding uncertainties
        lamp_wav: lamp library spectrum wavelegths
        lamp_spec: lamp library spectrum 
        linelist: lamp line list
    
    optional:
        upto: which orders are already done, (in the left and right direction from the starting order)
        trust_opto: do we completely trust the already done orders? if not, plots will be shown to you        

    output:
        global_wsol: the wavelength solution for each order of each arc frame
        params_wsol: The corresponding parameters to make the solution
    '''
    global_wsol = spec.copy()*0.0
    params_wsol = np.zeros((spec.shape[0],spec.shape[1],5))
    ##loop over arc frames
    for arc in range(spec.shape[0]):
        print "calibrating arc exposure " +str(arc+1)
        
    ##start at order 32 because I know what it is/can estimate the solution (at least for 
    ##the standard ACR/ALK setup):
        start_order  = 32  
        roughdisp    = 7000.0/60000.0/2.0
        wave_start   = 6957.0
        order        = spec[arc,start_order,:]
        roughwav     = wave_start + roughdisp*np.arange(len(order))
    
    ##do some cleaning for clearer plotting, in particular remove low points
        qwe            = np.where((lamp_wav >= np.min(roughwav-10)) & (lamp_wav <= np.max(roughwav+10)))[0] 
        ls             = lamp_spec[qwe] - np.min(lamp_spec[qwe])
        region         = np.where(ls < np.median(ls))
        ls[region]     = np.median(ls)
        norder         = order-np.min(order)
        region         = np.where(norder < np.median(norder))
        norder[region] = np.median(norder)    
    
    ##do the automatic calibration to get an initial fit for the known order
        fitpars,wsol,num_lines,wsol0 = order_wcal(roughwav,np.log10(norder),lamp_wav[qwe],np.log10(ls))

        global_wsol[arc,start_order,:]   = wsol
        params_wsol[arc,start_order,:]   = fitpars
    
    ##we now have a wavelength solution for a single order, we can extend it from here
    ##number of orders on each side of the starting order
        rord      = spec.shape[1] - start_order
        lord      = start_order
        order_gap = 28.933 ##the gap wavelength between orders in angstroms, starting value here was inspected by eye
        gap_shift = 0 
        
    ##start with the higher wavelength direction
        for i in range(start_order,spec.shape[1]):
            if upto[0] != -1:##if these are already done, load the solutions
                global_wsol = pickle.load(open(rdir+'global_wsol.pkl','rb'))
                params_wsol = pickle.load(open(rdir+'params_wsol.pkl','rb'))

            print 'Right Order ' + str(i)
            order    = spec[arc,i,:]
            ## if redoing the original known order, load to appropriate initial wavelength solution
            if i == start_order: 
                ppars    = params_wsol[arc,i,:].copy()
                roughwav = np.polyval(ppars,np.arange(spec.shape[2]))
            ## if on subsequent order, load the previous order solution
            if i != start_order:
                ppars    = params_wsol[arc,i-1,:].copy()
                ##if enough info is in the bank, add the solution delta (like back propagation but really dumb)
                if i > start_order+2: ppars += params_wsol[arc,i-1,:] ##- params_wsol[arc,i-2,:]
                ppars[4] = 0.0
                ##now offset by the predicted order gap
                roughwav = global_wsol[arc,i-1] - min(global_wsol[arc,i-1]) + max(global_wsol[arc,i-1]) +  order_gap        
            
            ##if one pass through the calibration is already done, use it for the starting position
            if (arc > 0) | ((upto[0] != -1) & (upto[0] >= i)): 
                roughwav =   global_wsol[np.max([arc-1,0]),i,:]
                knownpeaks = pickle.load(open(rdir+'peaks_ord_'+str(i)+'.pkl','rb'))
    
            ##clean the lamp and arc spectra for clearer plotting
            qwe    = np.where((lamp_wav >= np.min(roughwav-10)) & (lamp_wav <= np.max(roughwav+10)))[0] 
            ls     = lamp_spec[qwe] - np.median(lamp_spec[qwe])
            norder = order-np.median(order)
  
            ##do the interactive line fit, run the repass if order already done previously            
            if (arc > 0) | ((upto[0] != -1) & (upto[0] >= i)): 
                if trust_upto == False: fitpars,wsol,outpeaks = order_wcal_repass(roughwav,norder,lamp_wav[qwe],ls,linelist,knownpeaks,trustit=False)
                if trust_upto ==  True: fitpars,wsol,outpeaks = order_wcal_repass(roughwav,norder,lamp_wav[qwe],ls,linelist,knownpeaks,trustit=True)
                ## if not refitting:
                if len(fitpars) == 1:
                    fitpars = params_wsol[np.max([arc-1,0]),i,:]
                    wsol    = global_wsol[np.max([arc-1,0]),i,:]
                    outpeaks= knownpeaks
            ##if we haven't done this order yet
            else:fitpars,wsol,outpeaks= order_wcal_interact(roughwav,norder,lamp_wav[qwe],ls,linelist)
            
            ##store results
            global_wsol[arc,i,:] = wsol
            params_wsol[arc,i,:] = fitpars
            ##save progress as you go so you don't have to redo anything in the event of a fail
            pickle.dump(global_wsol,open(rdir+'global_wsol.pkl','wb'))
            pickle.dump(params_wsol,open(rdir+'params_wsol.pkl','wb'))
            pickle.dump(outpeaks,open(rdir+'peaks_ord_'+str(i)+'.pkl','wb'))

            #recalculate the gap between orders now that we have more data.
            ##also add a delta order gap (like back propagation but really dumb)
            if i > start_order:
                new_order_gap = np.min(wsol) - np.max(global_wsol[arc,i-1,:]) 
                gap_shift     = 0.0*(new_order_gap - order_gap)
                order_gap     = new_order_gap + gap_shift
      
            
        ## now go in the other direction:
        order_gap = -28.933
        for onum in range(1,lord+1):
            i = start_order - onum
            print 'Left Order ' + str(i)
            order    = spec[arc,i,:]
            ##rough wavelenngth solution
            roughwav = global_wsol[arc,i+1] -   max(global_wsol[arc,i+1]) + min(global_wsol[arc,i+1]) +order_gap

            
            ##if one pass through the calibration is already done, use it for the starting position
            ##and grab the positions of the peaks
            if (arc > 0) | ((upto[1] != -1) & (upto[1] <= i)): 
                roughwav   = global_wsol[np.min([arc-1,0]),i,:]
                knownpeaks = pickle.load(open(rdir+'peaks_ord_'+str(i)+'.pkl','rb'))
                
            ##clean things up for nice plotting 
            qwe            = np.where((lamp_wav >= np.min(roughwav-10)) & (lamp_wav <= np.max(roughwav+10)))[0] 
            ls             = lamp_spec[qwe] - np.median(lamp_spec[qwe])
            norder         = order-np.median(order)

            ##do the interactive line fit
            if (arc > 0) | ((upto[1] != -1) & (upto[1] <= i)): 
                if trust_upto == False: fitpars,wsol,outpeaks = order_wcal_repass(roughwav,norder,lamp_wav[qwe],ls,linelist,knownpeaks,trustit=False)
                if trust_upto ==  True: fitpars,wsol,outpeaks = order_wcal_repass(roughwav,norder,lamp_wav[qwe],ls,linelist,knownpeaks,trustit=True)
                ## if not refitting:
                if len(fitpars) == 1:
                    fitpars  = params_wsol[np.max([arc-1,0]),i,:]
                    wsol     = global_wsol[np.max([arc-1,0]),i,:]
                    outpeaks = knownpeaks
            ##if we haven't done this order yet
            else:fitpars,wsol,outpeaks= order_wcal_interact(roughwav,norder,lamp_wav[qwe],ls,linelist)
            
            ##store results
            global_wsol[arc,i,:] = wsol
            params_wsol[arc,i,:] = fitpars
            ##save progress as you go so you don't have to redo anything in the event of a fail
            pickle.dump(global_wsol,open(rdir+'global_wsol.pkl','wb'))
            pickle.dump(params_wsol,open(rdir+'params_wsol.pkl','wb'))
            pickle.dump(outpeaks,open(rdir+'peaks_ord_'+str(i)+'.pkl','wb'))

            #recalculate the gap between orders now that we have more data.
            ##also add a delta order gap (like back propagation but really dumb)
            new_order_gap = np.max(wsol) - np.min(global_wsol[arc,i+1,:]) 
            gap_shift     = new_order_gap - order_gap
            order_gap     = new_order_gap + gap_shift
        
    return global_wsol,params_wsol
    
def interpolate_wcal_timestamps(wcalib,wtime,otime):
    '''
    function to interpolate two bounding wavelength calibrations onto the timestamp of an
    target/object observation. e.g. if arcs were taken at 2am and 4am, but a target star 
    spectrum was taken at 3am, we want an interpolated wavelenght solution between the two arcs
    Just simple linear interpolation really.
    Returns a wavelength calibration for each target observation, as an array the same shape as spec

    input:
        wcalib: the wavelengths solution cube
        wtime: corresponding time stamps
        otime: target observatiosn times

    output:
        wsol_interp: interpolated wavelenght soliutions
    '''
    wsol_interp = np.zeros((len(otime),wcalib.shape[1],wcalib.shape[2]))
    for i in range(len(otime)):
        tdist   = otime[i]-wtime
        ##catch fails first:
        if ((np.max(wtime) < otime[i]) | (np.min(wtime) > otime[i])): 
            print 'Target Obs ' + str(i+1) + 'not bounded by arcs, using closest arc only'
            close = np.argmin(np.absolute(tdist))
            wsol_interp[i] = wcalib[close]
        else:
        ##find the nearest wavelength calibration on either side of the otime
            before  = np.where(tdist > 0)[0]
            after   = np.where(tdist < 0)[0]
            cbefore = before[np.argmin(np.absolute(tdist[before]))]
            cafter  = after[np.argmin(np.absolute(tdist[after]))]
            wsol_interp[i] = wcalib[cbefore] + (wcalib[cafter]-wcalib[cbefore])*(otime[i]-wtime[cbefore])/(wtime[cafter]-wtime[cbefore])
    return wsol_interp
  

def date_to_jd(year,month,day):
    '''
    calculate julian day from calendar day
    '''
    if month == 1 or month == 2:
        yearp = year - 1
        monthp = month + 12
    else:
        yearp = year
        monthp = month
    
    # this checks where we are in relation to October 15, 1582, the beginning
    # of the Gregorian calendar.
    if ((year < 1582) or
        (year == 1582 and month < 10) or
        (year == 1582 and month == 10 and day < 15)):
        # before start of Gregorian calendar
        B = 0
    else:
        # after start of Gregorian calendar
        A = np.trunc(yearp / 100.)
        B = 2 - A + np.trunc(A / 4.)
        
    if yearp < 0:
        C = np.trunc((365.25 * yearp) - 0.75)
    else:
        C = np.trunc(365.25 * yearp)
        
    D = np.trunc(30.6001 * (monthp + 1))
    
    jd = B + C + D + day + 1720994.5
    
    return jd  
   
def ut_convert(ut,utdate):
    '''
    Get observations timestamps in useable units from UT strings in coude headers
    '''
    hh    = np.zeros(len(ut))
    mm    = np.zeros(len(ut))
    ss    = np.zeros(len(ut)) 
    year  = np.zeros(len(ut)) 
    month = np.zeros(len(ut)) 
    day   = np.zeros(len(ut)) 
    julday = np.zeros(len(ut))
    ##pull the h/m/s out properly
    for i in range(len(ut)):
        sss = np.array(ut[i].split(':')).astype(float)
        hh[i]    = sss[0]
        mm[i]    = sss[1]
        ss[i]    = sss[2]
        dayfrac = (hh[i]*60.0*60.0+mm[i]*60.0+ss[i])/24.0/60.0/60.0
    ##do the same for the date:
        yyy = np.array(utdate[i].split('-')).astype(float)
        year[i]  = yyy[0]
        month[i] = yyy[1]
        day[i]   = yyy[2]
        julday[i] = date_to_jd(year[i],month[i],day[i]+dayfrac)
    return julday

def get_cosmics(cube,cube_snr,cutper = 95):
    '''
    function to find single pixel cosmics
    uses the derivative to identify the cosmics/ pixel problems on the base images
    !!!ACR never really finished getting this to work, so ignore it for now, it's not used 
    '''
    for i in range(cube.shape[0]):
        #pdb.set_trace()
        ##calculate the derivative in 2d over the image
        xgrad,ygrad = np.gradient(cube[i])
        pdb.set_trace()
        region = cube[i,0:1024,:].flatten()

        
        ##find gradient percentile cut level with numpy         
        px = np.percentile(np.absolute(xgrad[0:1024,:]),cutper)
        py = np.percentile(np.absolute(ygrad[0:1024,:]),cutper)  
        ##remove all points above this gradient level
        bad= np.where((np.absolute(xgrad)>px) | (np.absolute(ygrad) > py))
        ##make these high gradient point have no SNR 
        cube[i,bad[0],bad[1]]= np.median(cube[i])
        cube_snr[i,bad[0],bad[1]] = 0.000001
        pdb.set_trace()
        print "corrected " + str(len(bad[0])) + " bad pixels in this frame"
    return cube,cube_snr
    

def do_telluric(wav,spec,sig_spec,names,zd):
    '''
    the telluric removal code !!!This doesnt work yet!!!!
    ''' 
    ##go target by target
    fitter = TelluricFitter()
    fitter.SetObservatory('McDonald')
    humidity_guess = 50.0
    fitter.FitVariable({'h2o': humidity_guess,'o2': 2.12e5,'pressure':1016.0})   # This is a good initial guess for the O2 abundance
    fitter.SetBounds({'h2o': [1.0, 99.0],'o2': [5e4, 1e6]})
    
    
    for i in range(spec.shape[0]):
    ##loop order by order, fit the continuum, and the fit the tellurics
        i=-3 ## testing
        for ord in range(spec.shape[1]):
            ord = 10 ##testing
            cpars    = np.polyfit(wav[i,ord],spec[i,ord],2)
            thiscont = np.polyval(cpars,wav[i,ord])
            orderdat = DataStructures.xypoint(x=wav[i,ord]/10.0,y=spec[i,ord],err=sig_spec[i,ord],cont=thiscont)
            pdb.set_trace()
            ##now run the telluric fitter code
            ##66633.0
            fitter.AdjustValue({"angle": zd[i],"pressure": 3401.0,"resolution": 66633.0, "wavestart": orderdat.x[0] - 20.0,"waveend": orderdat.x[-1] + 20.0})
            telmodel = fitter.Fit(data=orderdat,adjust_wave='model',continuum_fit_order=2)
            pdb.set_trace()


def continuum_correct(spec,sig_spec,smooth_range=21): 
    '''
    Remove continuum from orders using a convex hull, doesnt work properly yet, but seems ok for 
    B/A-type stars 
    '''
    flat_spec = spec.copy()*0.0
    flat_sig  = spec.copy()*0.0
    continuum = spec.copy()*0.0
    

    nord = spec.shape[1]
    npix = spec.shape[2]
    xvect= np.arange(npix)
    shifter = (smooth_range-1)/2
    for i in range(spec.shape[0]): 
        for ord in range(nord):
           # print ord
            thisord = spec[i,ord]
            thiserr = sig_spec[i,ord]
            cord    = thisord.copy()
            ##first clean away bad points
            bad = np.where(thisord/thiserr <=1)[0]
            for b in range(len(bad)):
               # print b
                newspot = np.where(thisord[bad[b]+1:]/thiserr[bad[b]+1:] > 1)[0]
                if len(newspot) >0 : cord[bad[b]] = cord[bad[b]+1 +newspot[0]]
               # pdb.set_trace()
            
            ##smooth once on a long pass
            smoothord = smooth(cord,smooth_range,win='hanning')
            smoothord = smoothord[shifter:smoothord.shape[0]-shifter]
            ##now smooth again on a short pass
           # pdb.set_trace()
            smoothord2 = smooth(smoothord,5,win='hanning')
            smoothord2 = smoothord2[2:smoothord2.shape[0]-2]
           # smoothord2 = smoothord
            ##find local maxima
            maxspots = scipy.signal.argrelmax(smoothord2,order=10)[0]
            hullpoints = np.array([xvect[maxspots],smoothord2[maxspots]]).T            
            hull = scipy.spatial.ConvexHull(hullpoints,incremental=False)
            hullx = np.zeros(hull.simplices.shape[0])
            hully = hullx.copy()
            
            ##the convex hull simplices are in reverse (anticlockwise) order starting from the first point (then going to the last)
            for ss,vertex in enumerate(hull.vertices): 
             #   pdb.set_trace()
                if (vertex == 0) | (ss == hull.simplices.shape[0]-1):
                    #pdb.set_trace()
                    hullx[ss] = hullpoints[vertex,0]
                    hully[ss] = hullpoints[vertex,1]
                else:
                    ##exclude non-ordered cylce points
                    if hull.vertices[ss] > hull.vertices[ss+1]:
                        hullx[ss] = hullpoints[vertex,0]
                        hully[ss] = hullpoints[vertex,1]
                    else:
                        hullx[ss] = -99.9
                        
            ##now sort the points and remove any that were excluded above        
            good   = np.where(hullx >0)[0]
            hullx  = hullx[good]
            hully  = hully[good]        
            sorter = np.argsort(hullx)
            hullx  = hullx[sorter]
            hully  = hully[sorter]
            
            ##try fit a polynomial to the convex hull vertices
            ##the spline can improve 
            #hullpars = np.polyfit(hullx,hully,4)
            #hullpoly = np.polyval(hullpars,xvect)
            hullspl  = uvarspline(hullx,hully,k=2,s=20)
            splinefull = hullspl(xvect)
            
            
            ##now interpolate the convex hull onto the scale of the data
            full_hull        = np.interp(xvect,hullx,hully)
            flatord          = thisord/full_hull
            flat_spec[i,ord] = flatord
            continuum[i,ord] = full_hull
            flat_sig[i,ord]  = thiserr/np.absolute(full_hull)
            
            
            #plt.plot(hullpoints[:,0],hullpoints[:,1],'b.')
            #for simplex in hull.simplices: plt.plot(hullpoints[simplex, 0], hullpoints[simplex, 1], 'k-')
            
            
            ##testplot
            #plt.plot(xvect,thisord,'b')
            #plt.plot(xvect,smoothord2,'r')
            #plt.plot(hullpoints[:,0],hullpoints[:,1],'ro')
            #plt.plot(hullx,hully,'k-')            
            #plt.plot(xvect,full_hull,'k-',linewidth=2)
            #plt.plot(xvect,splinefull,'g-',linewidth=2)
            
            #if ord > 20 :pdb.set_trace()
            #pdb.set_trace()
    
    return flat_spec,flat_sig,continuum         


##!!!ACR STARTS HERE
#HERE IS WERE EVERYTHING GETS RUN, EVERYTHING ABOVE ARE FUNCTIONS THAT GET CALLED
##!!! start here

##standard inputs and options on what analysis parts you want, note working directory will be dir


##SET A WORKING AND CODE DIRECTORIES
#E.G.
#dir = '/path/to/data/'
#rdir = dir+'reduction/'
#codedir = 'path/to/this/code/'


##ACRs stuff
dir          = '/Volumes/UT2/UTRAID/mcd_alk/20140321/'
#rdir         = '/Volumes/UTRAID/mcd_alk/20140323/reduction/'
#codedir      = '/Users/arizz/python/projects/tull_coude/'
##for testing on aarons laptop 
#dir          = '/Users/aaron/data_local/mcd_alk/20140323/'
#dir          = '/Users/aaron/data_local/coude/161220/'
rdir         = dir+'reduction/'
codedir      = os.getenv("HOME")+'/python/projects/tull_coude/'
#dir          = '/Volumes/UT1/tullcoude/161015/'
#rdir         = dir+'reduction/'
#codedir      = '/Users/arizz/python/projects/tull_coude/'




##SET SOME PARAMETERS FOR THE RUN
##the headstrip file output from header_info.py script (found in the code directory)
infofile     = 'headstrip.csv'

##make master Bais and Flat, or just read in one that is already done
calsdone     = True
##show the bad pixel mask?
showbad      = False
##read all the arc/object data in? Can skip if extractions are already done
readcubes    = True
##Is the order trace already done?
tracedone    = True
##Have you extracted the data frames?
extractdone  = True
#extractdone  = True
extype=True ##set the quick keyword to True or False for the extraction (TRUE == QUICK)
##Have you extracted the arc frames?
wextractdone = True
##Have you Done the Wavelength calibration?
wcaldone     = False
#interpolate wavelength solutions onto object timestamp?
winterpdone  = False
#Continuum subtract/correct the spectra with a Convex Hull algorithm (doesn't work yet)?
flattendone  = True


##Do the telluric removal fit? This will save things as datastructures.xypoint variables
##this doesnt work yet!!
#telluricdone = False


##made up for the moment
##dark current per second of exposue
dcur = 0.0  ##find out about this

print "We are reducing directory" + dir 
print "Right???"
print "If yes, continue, if not, wtf get it together!!"
#runit = 1
runit = input("1=yes: ")
if runit != 1: pdb.set_trace()


##switch to the data dir
os.chdir(dir)


##!DODGY STUFF usually this should be commented out!!!!
##reformat the starting wavelength solution files, if you are using a previous night with a different number of arc exposures:
# wsol_template     = pickle.load(open(rdir+'global_wsol.pkl','rb'))[0]
# wfitpars_template = pickle.load(open(rdir+'params_wsol.pkl','rb'))[0]
# arcnumber         = 4
# wsol_start,pars_start = reshape_wavecal(wsol_template,wfitpars_template,arcnumber)
# pdb.set_trace()



##get data file information from the headstrip file
info = readcol(infofile,fsep=',',asRecArray=True)
dcur = info.ExpTime*dcur
##figure out which observations are bias/flat/arc/object
bias_ix = np.where( info.Type == 'zero')[0]
flat_ix = np.where( info.Type == 'flat')[0]
arc_ix  = np.where((info.Type == 'comp') & ((info.Object == 'Thar') | (info.Object == 'THAR') | (info.Object == 'A')))[0]
obj_ix  = np.where((info.Type == 'object') & (info.Object != "SolPort") & (info.Object != "solar port") & (info.Object != "solar_ort") & (info.Object != "test"))[0]


##QUICK LOOK OBSERVING MODE, JUST RUN THE LATEST FRAME
obj_ix = np.array([obj_ix[-1]]) 


## make the basic calibrations
if calsdone == False:
    ##create a superbias
    print "Reading Bias Files"
    superbias = build_bias(info.File[bias_ix])
    ##create flat field
    print "Reading Flat Files"
    flat_field = build_flat_field(info.File[flat_ix],superbias)
    print "Heres the bias, saving it to file"
    plt.imshow(np.log10(superbias),interpolation='none')
    pickle.dump(superbias,open(rdir+'bias.pkl','wb'))
    plt.show()
    print "Heres the Flat, saving it to file"
    plt.imshow(np.log10(flat_field),aspect='auto',interpolation='none')
    plt.colorbar()
    
    pickle.dump(flat_field,open(rdir+'flat.pkl','wb'))
    plt.show()

if calsdone == True:
    print "Reading premade Flat+Bias frames"
    superbias  = pickle.load(open(rdir+'bias.pkl','rb'))
    flat_field = pickle.load(open(rdir+'flat.pkl','rb'))

##make a first mask of bad pixels from the flat/bias    
badmask = make_badpix_mask(superbias,flat_field,99.9,showit=showbad)

##Read in and bias/flat the objects and arc frames
if readcubes == True:
    print "Reading Arc files"
    arc_cube,arc_snr = make_cube(info.File[arc_ix],info.rdn[arc_ix]/info.gain[arc_ix],dcur[arc_ix]/info.gain[arc_ix],bias=superbias)
    #raw_arc,raw_arc_snr  = make_cube(info.File[arc_ix],info.rdn[arc_ix]/info.gain[arc_ix],dcur[arc_ix]/info.gain[arc_ix])
    
    print "Reading Object files, and bias/flat correcting + bad pixel correcting"
    obj_cube,obj_snr = make_cube(info.File[obj_ix],info.rdn[obj_ix]/info.gain[obj_ix],dcur[obj_ix]/info.gain[obj_ix],
                                 bias=superbias,flat=flat_field,bpmask=badmask)

    ##raw version for testing
    #raw_obj,raw_obj_snr  = make_cube(info.File[obj_ix],info.rdn[obj_ix]/info.gain[obj_ix],dcur[obj_ix]/info.gain[obj_ix],bias=None,flat=None)
    #obj_cube = raw_obj
    #obj_snr = raw_obj_snr
    

##now need to extract the spectrum for the objects and arc frames
##do a first trace for the flat_field, then do individual trace for every object, 
##make the trace
orderstart = -32 ##the pixel position that the orders start at. e,g skip the overscan region
##note you can load a trace from another night, and check if it's fine
if tracedone == False:
    order_zeros,ovals = start_trace(flat_field[:,orderstart])
    print "Heres the zero trace"
    plt.plot(flat_field[:,orderstart])
    plt.plot(order_zeros,ovals,'or')
    plt.show()

##now that we have a zero point, run pixel by pixel through each 
##data cube and build the trace data
    thetrace = full_trace(obj_cube,order_zeros,orderstart)
    medtrace = np.median(thetrace,axis=0)
    pickle.dump(medtrace,open(rdir+'median_trace.pkl','wb'))
    print "Heres the median image trace, saving it"
    plt.imshow(np.log10(flat_field),cmap=plt.get_cmap('gray'))
    for i in range(len(order_zeros)) : plt.plot(medtrace[i,:],'r')
    plt.show()
if tracedone == True: 
    print "Reading premade Trace"
    medtrace = pickle.load(open(rdir+"median_trace.pkl","rb"))
    if extractdone == False:
        print "Heres the premade Trace, does it look fine?"
        plt.imshow(np.log10(flat_field),cmap=plt.get_cmap('gray'))
        for i in range(medtrace.shape[0]) : plt.plot(medtrace[i,:],'r')
        plt.show()
    
    
##now that we have the trace, we can extract everything.
if extractdone == False:
    spec,sig_spec = extractor(obj_cube,obj_snr,medtrace,quick=extype,arc=False,nosub=False)
    pickle.dump(spec,open(rdir+'extracted_spec.pkl','wb'))
    pickle.dump(sig_spec,open(rdir+'extracted_sigspec.pkl','wb'))
if extractdone == True:
    print "Reading premade extracted spectra"
    spec     = pickle.load(open(rdir+'extracted_spec.pkl','rb'))
    sig_spec = pickle.load(open(rdir+'extracted_sigspec.pkl','rb'))

##now extract the wavelength calibration frames
if wextractdone == False:
    wspec,sig_wspec = extractor(arc_cube,arc_snr,medtrace,quick=True,nosub=True,arc=True)
    pickle.dump(wspec,open(rdir+'extracted_wspec.pkl','wb'))
    pickle.dump(sig_wspec,open(rdir+'extracted_sigwspec.pkl','wb'))
if wextractdone == True:
    print "Reading premade extracted Arcs"
    wspec = pickle.load(open(rdir+'extracted_wspec.pkl','rb'))
    sig_wspec = pickle.load(open(rdir+'extracted_sigwspec.pkl','rb'))

##note, the orders are in backwards arrangement, e.g. 16 should be before 15 and so on 
##rearrenge the orders
wspec      = wspec[:,::-1,:]
sig_wspec  = sig_wspec[:,::-1,:]
spec       = spec[:,::-1,:]
sig_spec   = sig_spec[:,::-1,:]

##Gather everything for the wavelength calibration
##read in the ThAr  lamp spectrum and exact linelist
##you can change what you want to use for the calibration here.
hdulist    = fits.open(codedir + './thar_photron.fits')
head       = hdulist[0].header
lamp_spec  = hdulist[0].data
lamp_w1    = head["CRVAL1"]
lamp_delta = head["CDELT1"]
lamp_wav   = np.arange(len(lamp_spec))*lamp_delta+lamp_w1
thar       = readcol(codedir + 'ThAr_list.txt',asRecArray=True)

##run the wavelength calibration, requires user input unfortunately
if wcaldone == False: wcalib, wfitpars = wavecal(wspec,sig_wspec,lamp_wav,lamp_spec,thar.wav,upto=(-1,-1),trust_upto=True)
if wcaldone ==  True:
    wcalib   = pickle.load(open(rdir+'global_wsol.pkl','rb'))
    wfitpars = pickle.load(open(rdir+'params_wsol.pkl','rb'))

##now interpolate the wavelength solutions onto the timestamps of the target observations
julday = ut_convert(info.UT,info.UTdate)
##now interpolate properly
if winterpdone == False:
    print "Interpolating Wavelength Solution onto the Timestamp of each target observation"
    final_wsol= interpolate_wcal_timestamps(wcalib,julday[arc_ix],julday[obj_ix])
    print "Saving Target specific Wavelength Calibration"
    pickle.dump(final_wsol,open(rdir+'wsol_final.pkl','wb'))
    print "Outputting Target Object Names"
    pickle.dump(info.Object[obj_ix],open(rdir+'target_names.pkl','wb'))
if winterpdone == True:
    print "Reading target specific wavelength calibrations"
    final_wsol = pickle.load(open(rdir+'wsol_final.pkl','rb'))



##Stopping here for now
pdb.set_trace()







##do continuum removal step
if flattendone == False:
    print 'Continuum Correcting the Spectra'
    flat_spec,flat_sig,continuum = continuum_correct(spec,sig_spec)
    ##save the output 
    pickle.dump(flat_spec,open(rdir+'final_spec.pkl','wb'))
    pickle.dump(flat_sig,open(rdir+'final_sigspec.pkl','wb'))
    pickle.dump(continuum,open(rdir+'final_continuum.pkl','wb'))
else:
    print 'Reading Premade Continuum Corrected Spectra' 
    flat_spec = pickle.load(open(rdir+'final_spec.pkl','rb'))
    flat_sig  = pickle.load(open(rdir+'final_sigspec.pkl','rb'))
    continuum = pickle.load(open(rdir+'final_continuum.pkl','rb'))

#if telluricdone == False:
#    do_telluric(final_wsol,spec,sig_spec,info.Object[obj_ix],info.zenith[obj_ix])
#    pdb.set_trace()
    


print "!!!Analysis Finished!!!"
pdb.set_trace()
print "Im Here"
