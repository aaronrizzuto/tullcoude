##script to read data file headers and output a table of useful information

import numpy as np
import pdb
from astropy.io import fits
import glob, os




##the data directory
dir = '/Users/aaron/data_local/coude/161220/'
#dir     = '/Volumes/UTRAID/mcd_alk/20140323/'

outname = 'headstrip.csv'

os.chdir(dir)
files = glob.glob("*.fits")


outfile = open(outname,"wb")
heading = 'File,Object,RA,DEC,Type,ExpTime,Order,Airmass,UTdate,UT,gain,rdn,zenith \n'
outfile.write(heading)
for i in range(len(files)):
    hdulist    = fits.open(files[i])
    head       = hdulist[0].header
    #pdb.set_trace()
    itype   = head["imagetyp"]
    if len(files[i].split('.')) > 2: itype = "unknown"
    ra = ''
    dec = ''
    if "RA"  in head.keys(): ra      = head["RA"]
    if "DEC" in head.keys(): dec     = head["DEC"]
    order   = head["order"]
    air = '100'
    if "airmass" in head.keys(): air     = str(head["airmass"])
    UT      = head["UT"]
    exptime = str(head["exptime"])
    gain = str(head["gain3"])
    object  = head["object"]
    rdn     = str(head["rdnoise3"])
    utdate  = head["DATE-OBS"]
    zd = head["ZD"]
    line = files[i] +','+object + ','+ ra +','+ dec+','+ itype+','+exptime+','+order+','+ air+','+utdate+','+ UT +','+gain+','+rdn+','+zd+' \n' 
    outfile.write(line)
    outfile.flush()



outfile.close()
    